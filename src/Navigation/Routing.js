import * as React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {
  createStackNavigator,
  TransitionPresets,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';

import {
  View,
  StatusBar,
  TouchableOpacity,
  SafeAreaView,
  Text,
  Button,
} from 'react-native';
import {
  LauncherContainer,
  DashboardContainer,
  HomeContainer,
  RegisterContainer
} from '../Navigation';
import {AsyncStorage} from 'react-native';

const config = {
  animation: 'spring',
  config: {
    stiffness: 1000,
    damping: 50,
    mass: 3,
    overshootClamping: true,
    restDisplacementThreshold: 0.01,
    restSpeedThreshold: 0.01,
  },
};

const Stack = createStackNavigator();
function Nav() {
  return (
    <Stack.Navigator
      initialRouteName="DashboardContainer"
      screenOptions={{
        gestureEnabled: false,
        gestureDirection: 'horizontal',
        ...TransitionPresets.SlideFromRightIOS,
      }}
      headerMode="float"
      animation="fade">
      <Stack.Screen
        name="DashboardContainer"
        component={DashboardContainer}
        options={{headerShown: false}}
      />
       <Stack.Screen
        name="RegisterContainer"
        component={RegisterContainer}
        options={{headerShown: true}}
      />
    </Stack.Navigator>
  );
}

const Navigator = createSwitchNavigator(
  {
    LauncherContainer: {screen: LauncherContainer},
    Nav: {screen: Nav},
  },
  {
    initialRouteName: 'LauncherContainer',
    defaultNavigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
);

const AppNavigator = createAppContainer(Navigator);

export default AppNavigator;
