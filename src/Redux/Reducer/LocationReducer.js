/* eslint-disable prettier/prettier */
import ACTION_TYPES from './../Constants/ActonTypes';

// TODO
/* Initialize your global state variables
   EX : adding parameters like color, text, util */
const initialState = {
  Location: '',
};

const LocationReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_TYPES.LOCATION_ITEM_UPDATE:
      return {
        ...state,
        Location: action.data,
      };
    default:
      return state;
  }
};

export default LocationReducer;
