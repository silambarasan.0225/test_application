/* eslint-disable prettier/prettier */
import ACTION_TYPES from './../Constants/ActonTypes'

// TODO
/* Initialize your global state variables
   EX : adding parameters like color, text, util */
const initialState = {
    comment : ''
};

const CommentReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_TYPES.FEEDBACK_UPDATE_USER_COMMENT:
            return {
                ...state,
                comment  : action.data
            };
        default:
            return state
    }
}

export default CommentReducer;
