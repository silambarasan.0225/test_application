/* eslint-disable prettier/prettier */
import ACTION_TYPES from './../Constants/ActonTypes'

// TODO
/* Initialize your global state variables
   EX : adding parameters like color, text, util */
const initialState = {
    insident : ''
};

const IncidentReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_TYPES.INCIDENT_ITEM_UPDATE:
            return {
                ...state,
                insident  : action.data
            };
        default:
            return state
    }
}

export default IncidentReducer;
