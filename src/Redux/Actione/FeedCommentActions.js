/* eslint-disable prettier/prettier */
import ACTION_TYPES from '../Constants/ActonTypes'

export const updateUserComment = (comment_data) => {
    return {
        type: ACTION_TYPES.FEEDBACK_UPDATE_USER_COMMENT,
        data: comment_data
    }
};

