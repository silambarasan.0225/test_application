/* eslint-disable prettier/prettier */
import ACTION_TYPES from '../Constants/ActonTypes';

export const LocationUpdate = comment_data => {
  return {
    type: ACTION_TYPES.LOCATION_ITEM_UPDATE,
    data: comment_data,
  };
};
