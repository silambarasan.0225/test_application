/* eslint-disable prettier/prettier */

const colors = {
  text: '#B6D0D2',
  button: '#3694E7',
  redcoler: '#FA3636',
  white: '#FDFEFD',

  // discount_label : '#4ABF61',
  // tabInActive : 'rgba(0, 0, 0, 0.4)',
  // appRedColor : '#CC373A',
  // appGreenColor : '#4DAF4E',
  // appMediumGreenColour : '#558B2F',
  // greyE5 : '#E5E5E5',
  // borderBackground : 'rgba(29, 29, 38, 0.2)',
  // greyEA : '#EAECEE',
  // buttonBorder : '#808B96',
  // errorRed : '#EF5159',
  // iosBlue : 'rgba(0,122,255,1)',
  // statusBarGrey : '#ecf0f1',
  // blueGrey : '#F4F4F4',
  // midnightGrey : '#6D6C6C',
  // background: '#1F0808',
  // clear: 'rgba(0,0,0,0)',
  // textColorBlack : '#313131',
  // inputLine: '#e7e7e7',
  // facebook: '#3b5998',
  // grey : '#E5E5E5',
  // backgroundBlack : '#2f2f2f',
  // transparent: 'rgba(0,0,0,0)',
  // silver: '#F7F7F7',
  // steel: '#CCCCCC',
  // error: 'rgba(200, 0, 0, 0.8)',
  // saltpaper : 'rgba(255,255,255,0.5)',
  // ricePaper: 'rgba(255,255,255, 0.75)',
  // frost: '#D8D8D8',
  // cloud: 'rgba(200,200,200, 0.35)',
  // windowTint: 'rgba(0, 0, 0, 0.4)',
  // panther: '#161616',
  // charcoal: '#595959',
  // coal: '#2d2d2d',
  // bloodOrange: '#fb5f26',
  // snow: 'white',
  // ember: 'rgba(164, 0, 48, 0.5)',
  // fire: '#e73536',
  // drawer: 'rgba(30, 30, 29, 0.95)',
  // eggplant: '#251a34',
  // border: '#483F53',
  // banner: '#5F3E63',
  // titleText : '#3c3b3b',
  // buttonBlue : '#199ad7',
  // white: '#FFF',
  // socailBorder : '#d0d0d0',
  // greyText: '#6a6a6a',
  // tBlue: '#2A94cc',
  // lBlue: '#1A99E8',
  // listBorder : '#1d1d26',
  // semiGrey : '#f5f5f5',
  // logRed : '#d51b1b',
  // nearlyWhite : '#f3f3f4',
  // ratingBlue : '#36bcfc',
  // nearTransparent : 'rgba(0,0,0,0.08)',
  // whiteBlur : 'rgba(255,255,255,0.8)',
  // mildBlack : '#222222',
  // underlineBlue : '#0000FF',
  // windowTrans: 'rgba(0, 0, 0, 0.4)',

  // //designer input
  // searchBorder : '#BFBFBF',
  // newBorderGrey : 'rgba(142, 142, 147, 1)',
  // newBorder : 'rgba(142, 142, 147, 0.5)',
  // pureBlack : '#000000',
  // halfRedBorder : 'rgba(204, 55, 57, 0.2)',
  // transNewRed : '#FCF5F5',
  // newAppRed : '#CC3739',
  // yello : '#FFD600',
  // saltGrey : '#c4c4c4',
  // orderGreen : '#A5D476',
  // orderYello : '#F8B416',
  // newBlacky : '#4A4A4A',
  // drawerBg : '#FAFAFA',
  // airblack : '#484848',
  // thikeGrey: "#6b6b6b",

  // purpler : '#A72DBB',
  // greyF4 : '#F4F4F4',

  // gradient3 : '#FED337',
  // gradient4 : '#ED2027',
  // gradient1 : '#000000',
  // gradient2 : '#000000',
  // appCore1  : '#d1021b',

  // empire_header_red : '#d1021b',
  // empire_background: '#f6f2e7',
  // empire_blue : '#2897ef',
  // empire_green : '#228B22',
  // // empire_green : '#1781bd',
  // empire_thick_border : '#E5E5E5',
  // empire_border_grey : '#bebebe',
  // empire_light_grey : '#f2f2f2'
};

export default colors;
