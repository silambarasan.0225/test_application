/* eslint-disable prettier/prettier */
import {PixelRatio, Dimensions} from 'react-native'

const pixelRatio = PixelRatio.get();

const {width, height} = Dimensions.get('window');

const calcSize = (size) => {
    if (pixelRatio >= 2 && pixelRatio < 3) {
        // iphone 5s and older Androids
        if (width < 360) {
            return size;
        }
        // iphone 5
        if (height < 667) {
            return size;
            // iphone 6-6s
        } else if (height >= 667 && height <= 735) {
            return size;
        }
        // older phablets
        return size;
    } else if (pixelRatio >= 3 && pixelRatio < 3.5) {
        // catch Android font scaling on small machines
        // where pixel ratio / font scale ratio => 3:3
        if (width <= 360) {
            return size;
        }
        // Catch other weird android width sizings
        if (height < 667) {
            return size;
            // catch in-between size Androids and scale font up
            // a tad but not too much
        }
        if (height >= 667 && height <= 735) {
            return size;
        }
        // catch larger devices
        // ie iphone 6s plus / 7 plus / mi note 等等
        return size;
    } else if (pixelRatio >= 3.5) {
        // catch Android font scaling on small machines
        // where pixel ratio / font scale ratio => 3:3
        if (width <= 360) {
            return size;
            // Catch other smaller android height sizings
        }
        if (height < 667) {
            return size;
            // catch in-between size Androids and scale font up
            // a tad but not too much
        }
        if (height >= 667 && height <= 735) {
            return size;
        }
        // catch larger phablet devices
        return size;
    } else
    // if older device ie pixelRatio !== 2 || 3 || 3.5
        return size;
}

export default {calcSize}