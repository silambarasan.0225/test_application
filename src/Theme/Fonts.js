/* eslint-disable prettier/prettier */
import Metrics from './Metrics';
import Colors from './Colors';
import colors from './Colors';

const type = {
  black: 'OpenSans-Bold',
  light: 'OpenSans-BoldItalic',
  heavy: 'OpenSans-ExtraBold',
  book: 'OpenSans-Book',

  mont_black: 'OpenSans-Italic',
  mont_bold: 'OpenSans-ExtraBoldItalic',
  mont_light: 'OpenSans-Light',
  mont_medium: 'OpenSans-LightItalic',
  mont_reg: 'OpenSans-Regular',
  mont_semi: 'OpenSans-SemiBold',

  // mont_black : 'Montserrat-Black',
  // mont_bold : 'Montserrat-Bold',
  // mont_light : 'Montserrat-Light',
  // mont_medium : 'Montserrat-Medium',
  // mont_reg : 'Montserrat-Regular',
  // mont_semi : 'Montserrat-SemiBold',
};

const size = {
  medium: Metrics.calcSize(24),
  large: Metrics.calcSize(26),
  xl: Metrics.calcSize(28),
  xxl: Metrics.calcSize(30),
  h1: Metrics.calcSize(22),
  h2: Metrics.calcSize(20),
  h3: Metrics.calcSize(18),
  h4: Metrics.calcSize(16),
  h5: Metrics.calcSize(14),
  h6: Metrics.calcSize(12),
  reg: Metrics.calcSize(14),
  sm: Metrics.calcSize(13),
  vsm: Metrics.calcSize(10),
  msm: Metrics.calcSize(11),
  minute: 10.5,
  cartInidc: 14,
  micro: 8,
};

const colorStyle = {
  newAppRed: {
    color: Colors.newAppRed,
  },
  empireRed: {
    color: Colors.empire_header_red,
  },
  empireGreen: {
    color: Colors.empire_green,
  },
  appRed: {
    color: Colors.appRedColor,
  },
  white: {
    color: Colors.white,
  },
  appGreen: {
    color: Colors.appGreenColor,
  },
  black: {
    color: Colors.textColorBlack,
  },
  mildBlack: {
    color: Colors.mildBlack,
  },
  underlineBlue: {
    color: Colors.underlineBlue,
  },
  iosBlue: {
    color: Colors.iosBlue,
  },
  redColor: {
    color: Colors.errorRed,
  },
  grey: {
    color: Colors.greyText,
  },
  tabInActive: {
    color: Colors.tabInActive,
  },
  pureBlack: {
    color: Colors.pureBlack,
  },
  borderGrey: {
    color: Colors.newBorderGrey,
  },
  hideTextGrey: {
    color: Colors.searchBorder,
  },
  saltGrey: {
    color: Colors.saltGrey,
  },
  orderGreen: {
    color: colors.orderGreen,
  },
  thikeGrey: {
    color: colors.thikeGrey,
  },
  orderYello: {
    color: Colors.orderYello,
  },
  airblack: {
    color: Colors.airblack,
  },
  purpler: {
    color: Colors.purpler,
  },
  appCore1: {
    color: Colors.appCore1,
  },
};

const style = {
  medium: {
    fontSize: size.medium,
    fontFamily: type.black,
  },
  large: {
    fontSize: size.large,
    fontFamily: type.black,
  },
  xl: {
    fontSize: size.xl,
    fontFamily: type.black,
  },
  xxl: {
    fontSize: size.xxl,
    fontFamily: type.black,
  },
  h1: {
    fontSize: size.h1,
    fontFamily: type.black,
  },
  h2: {
    fontSize: size.h2,
    fontFamily: type.black,
  },
  h3: {
    fontSize: size.h3,
    fontFamily: type.black,
  },
  h4: {
    fontSize: size.h4,
    fontFamily: type.black,
  },
  h5: {
    fontSize: size.h5,
    fontFamily: type.black,
  },
  h6: {
    fontSize: size.h6,
    fontFamily: type.black,
  },
  reg: {
    fontSize: size.reg,
    fontFamily: type.light,
  },
  small: {
    fontSize: size.sm,
    fontFamily: type.light,
  },
  vsmall: {
    fontSize: size.vsm,
    fontFamily: type.light,
  },
  headerTitle: {
    fontSize: size.h5,
    fontFamily: type.book,
    color: Colors.textColorBlack,
  },
  greyText: {
    color: Colors.greyText,
  },
  bookMicro: {
    fontSize: size.micro,
    fontFamily: type.book,
  },
  bookH1: {
    fontSize: size.h1,
    fontFamily: type.book,
  },
  bookH2: {
    fontSize: size.h2,
    fontFamily: type.book,
  },
  bookH3: {
    fontSize: size.h3,
    fontFamily: type.book,
  },
  bookH4: {
    fontSize: size.h4,
    fontFamily: type.book,
  },
  bookH5: {
    fontSize: size.h5,
    fontFamily: type.book,
  },
  bookH6: {
    fontSize: size.h6,
    fontFamily: type.book,
  },
  bookmsm: {
    fontSize: size.msm,
    fontFamily: type.book,
  },
  lightH1: {
    fontSize: size.h1,
    fontFamily: type.light,
  },
  lightH2: {
    fontSize: size.h2,
    fontFamily: type.light,
  },
  lightH3: {
    fontSize: size.h3,
    fontFamily: type.light,
  },
  lightH4: {
    fontSize: size.h4,
    fontFamily: type.light,
  },
  lightH5: {
    fontSize: size.h5,
    fontFamily: type.light,
  },
  lightH6: {
    fontSize: size.h6,
    fontFamily: type.light,
  },
  lightvsm: {
    fontSize: size.vsm,
    fontFamily: type.light,
  },
  heavyH1: {
    fontSize: size.h1,
    fontFamily: type.heavy,
  },
  heavyH2: {
    fontSize: size.h2,
    fontFamily: type.heavy,
  },
  heavyH3: {
    fontSize: size.h3,
    fontFamily: type.heavy,
  },
  heavyH4: {
    fontSize: size.h4,
    fontFamily: type.heavy,
  },
  heavyH5: {
    fontSize: size.h5,
    fontFamily: type.heavy,
  },
  minute: {
    fontSize: size.minute,
    fontFamily: type.light,
  },
  micro: {
    fontSize: size.micro,
    fontFamily: type.light,
  },
  cartInidc: {
    fontSize: size.cartInidc,
    fontFamily: type.light,
  },

  mont_bold_h1: {
    fontSize: size.h1,
    fontFamily: type.mont_bold,
  },
  mont_bold_h2: {
    fontSize: size.h2,
    fontFamily: type.mont_bold,
  },
  mont_bold_h3: {
    fontSize: size.h3,
    fontFamily: type.mont_bold,
  },
  mont_bold_h4: {
    fontSize: size.h4,
    fontFamily: type.mont_bold,
  },
  mont_bold_h5: {
    fontSize: size.h5,
    fontFamily: type.mont_bold,
  },
  mont_bold_h6: {
    fontSize: size.h6,
    fontFamily: type.mont_bold,
  },

  mont_medium_h2: {
    fontSize: size.h2,
    fontFamily: type.mont_medium,
  },
  mont_medium_h4: {
    fontSize: size.h4,
    fontFamily: type.mont_medium,
  },
  mont_medium_h5: {
    fontSize: size.h5,
    fontFamily: type.mont_medium,
  },
  mont_medium_h6: {
    fontSize: size.h6,
    fontFamily: type.mont_medium,
  },

  mont_semibold_h6: {
    fontSize: size.h6,
    fontFamily: type.mont_semi,
  },
  mont_semibold_h5: {
    fontSize: size.h5,
    fontFamily: type.mont_semi,
  },
  mont_semibold_h4: {
    fontSize: size.h4,
    fontFamily: type.mont_semi,
  },
  mont_semibold_h2: {
    fontSize: size.h2,
    fontFamily: type.mont_semi,
  },
  mont_semibold_h1: {
    fontSize: size.h1,
    fontFamily: type.mont_semi,
  },
  mont_semibold_vsm: {
    fontSize: size.vsm,
    fontFamily: type.mont_semi,
  },
  mont_reg_vsm: {
    fontSize: size.vsm,
    fontFamily: type.mont_reg,
  },
  mont_reg_h3: {
    fontSize: size.h3,
    fontFamily: type.mont_reg,
  },
  mont_reg_h4: {
    fontSize: size.h4,
    fontFamily: type.mont_reg,
  },
  mont_reg_h5: {
    fontSize: size.h5,
    fontFamily: type.mont_reg,
  },
  mont_reg_h6: {
    fontSize: size.h6,
    fontFamily: type.mont_reg,
  },
};

export default {
  type,
  size,
  style,
  colorStyle,
};
