/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  ActivityIndicator,
  FlatList,
} from 'react-native';

import {styles} from './styles';
import {Fonts, Colors} from '../../Theme';
import colors from '../../Theme/Colors';
import axios from 'axios';
import openMap from 'react-native-open-maps';
import {url} from '../../Service/Configuration/config';
import {connect} from 'react-redux';
import {LocationUpdate} from './../../Redux/Actione/LocationAction';
const {
  mont_semibold_h5,
  mont_semibold_h3,
  mont_semibold_h6,
  mont_semibold_h1,
  mont_semibold_h2,
} = Fonts.style;
const {button, white} = Fonts.colorStyle;

const RenderItem = props => {
  return (
    <TouchableOpacity
      style={{
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 5,
        backgroundColor: colors.text,
        flexDirection: 'row',
        height: 100,
      }}
      onPress={() => props.deletitem(props.data)}>
      <View style={{flex: 1}}>
        <View style={styles.listdata}>
          <Text style={[mont_semibold_h2, styles.listtext]}>
            {props.data.properties.type}
          </Text>
        </View>
        <View style={styles.listdatas}>
          <View
            style={{
              flex: 1,
              alignItems: 'flex-start',
              justifyContent: 'center',
            }}>
            <Text style={[mont_semibold_h6, styles.listtext]}>
              {'Lat = ' + props.data.geometry.coordinates[0]}
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              alignItems: 'flex-start',
              justifyContent: 'center',
            }}>
            <Text style={[mont_semibold_h6, styles.listtext]}>
              {'Lan = ' + props.data.geometry.coordinates[1]}
            </Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};
const Loadedata = props => {
  let renderData = props.datas;

  return (
    <View style={{flex: 1}}>
      <View style={{flex: 1, paddingBottom: 10}}>
        <FlatList
          data={renderData}
          onEndReachedThreshold={0.1}
          initialNumToRender={10} // Reduce initial render amount
          maxToRenderPerBatch={1} // Reduce number in each render batch
          maxToRenderPerBatch={100}
          windowSize={7}
          renderItem={({item, index}) => (
            <RenderItem data={item} deletitem={props.deletitem} />
          )}
          keyExtractor={() => props.keyExtractor}
        />
      </View>
    </View>
  );
};

class LocationContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }

  async componentDidMount() {
    this.setState({loading: true});
    try {
      const response = await axios.get(
        url + ':443/api/v2/locations?proximity_square=100',
      );

      this.props.LocationUpdate(response.data.features);
      this.setState({
        allItem: response && response.data && response.data.features,
        loading: false,
      });
    } catch (error) {
      alert(error.message);
    }
  }

  deletitem = listdata => {
    openMap({
      latitude: listdata.geometry.coordinates[0],
      longitude: listdata.geometry.coordinates[1],
    });
  };

  extractKey = ({id}) => id;

  render() {
    const {Location} = this.props.LocationReducerState;
    return (
      <View style={styles.container}>
        <Loadedata
          keyExtractor={this.extractKey}
          datas={Location}
          onFormSubmit={this.ViewData}
          renderItem={this.renderItem}
          deletitem={this.deletitem}
        />

        {this.state.loading && (
          <View>
            <ActivityIndicator size="small" color="red" />
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  LocationReducerState: state.LocationReducer,
});

const mapDispatchToProps = dispatch => ({
  LocationUpdate: data => {
    dispatch(LocationUpdate(data));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LocationContainer);
