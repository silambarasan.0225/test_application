import {StyleSheet} from 'react-native';
import {Colors} from '../../Theme';


export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    height: 40,
    width: '100%',
    padding: 10,
  },
  headertitle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textdata: {
    height: 40,
    alignItems: 'flex-start',
    justifyContent: 'center',
    width: '100%',
    backgroundColor: 'pink',
    marginTop: 5,
    borderRadius: 5,
  },
  tinyLogo: {
    width: 20,
    height: 20,
  },
  listdata: {
    height: 40,
    flex: 1,
    justifyContent: 'center',
  },
  listdatas:{
    flexDirection:"row",
    height: 40,
    width:"100%",
    flex: 1,
    alignItems:"center",
    justifyContent: 'center',
  },
  refresh: {
    height: 40,
    width: '100%',
    paddingRight: 10,
    alignItems: 'center',
    justifyContent: 'center',

    borderRadius: 10,
  },
  image: {
    width: '97%',
    height: '97%',
    alignItems:"center",
    justifyContent:"center",
    borderRadius: 10,
    resizeMode: 'contain',

  },
  listtext: {
    padding: 10,
  },
  button: {
    height: 40,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.button,
    borderRadius: 5,
  },
  refbutton: {
    height: 40,
    width: '100%',
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.button,
    borderRadius: 5,
  },
});
