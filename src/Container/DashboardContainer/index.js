/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
  View,
  StatusBar,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  Text,
  Button,
  Image,
  Animated,
  SlideDown,
  BackHandler,
  TextInput,
  ScrollView,
  FlatList,
} from 'react-native';
import {
  Header,
  LearnMoreLinks,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {styles} from './styles';
import {AsyncStorage} from 'react-native';
import {Fonts, Colors} from './../../Theme';
import colors from '../../Theme/Colors';
import {NavigationContainer} from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';

import Incident from '../IncidentContiner';
import locations from '../LocationContainer';

const {
  mont_semibold_h5,
  mont_semibold_h4,
  mont_semibold_h6,
  mont_semibold_h1,
} = Fonts.style;
const {button, white} = Fonts.colorStyle;

const Tab  = createMaterialTopTabNavigator();

function MyTab(Root) {
  return (
    <Tab.Navigator 
    
      tabBarOptions={{
      labelStyle: { fontSize: 15 },
      style: { Fonts: mont_semibold_h4 },

    }}>
      <Tab.Screen name="Incidents" component={Incident} />
      <Tab.Screen name="locations" component={locations} />
    </Tab.Navigator>
  );
}

class DashboardContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      BackHandler.exitApp();
      return true;
    });
  }
  menu() {
    this.props.navigation.openDrawer();
  }

  render() {   
    return <MyTab />;
  }
}

export default DashboardContainer;
