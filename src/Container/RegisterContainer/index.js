/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {View, ActivityIndicator, Text, Image, BackHandler} from 'react-native';

import {styles} from './styles';
import {Fonts, Colors} from './../../Theme';
import axios from 'axios';
import {url} from '../../Service/Configuration/config';
import {connect} from 'react-redux';
import {insidentupdate} from './../../Redux/Actione/InsidentAction';
const {
  mont_semibold_h5,
  mont_semibold_h4,
  mont_semibold_h2,
  mont_semibold_h1,
} = Fonts.style;
const {button, white} = Fonts.colorStyle;

class RegisterContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  async componentDidMount() {
    this.props.insidentupdate({});
    const {listdata} = this.props.route.params;
    this.setState({loading: true});
    try {
      const response = await axios.get(
        url + ':443/api/v2/incidents/' + listdata.id,
      );
      this.props.insidentupdate(response.data.incident);

      this.setState({loading: false});
    } catch (error) {
      // handle error
      alert(error.message);
    }
  }
  componentWillMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    this.props.navigation.goBack();
    return true;
  }

  render() {
    const {insident} = this.props.IncidentReducerState;

    this.props.navigation.setOptions({
      headerTitle: () => (
        <View style={styles.headertitle}>
          <Text style={[mont_semibold_h1]}>Incidents</Text>
        </View>
      ),
      headerRight: () => (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            paddingRight: 10,
          }}>
          {/* <Text>Login</Text> */}
        </View>
      ),
    });
    return (
      <View style={styles.container}>
        <View style={{height: 10, width: '100%'}} />
        <View numberOfLines={2} style={styles.login}>
          <Text style={[mont_semibold_h2, styles.underline]}>
            {insident.title}
          </Text>
        </View>
        <View style={styles.email_username}>
          <Text numberOfLines={3} style={[mont_semibold_h5]}>
            {insident.description}
          </Text>
        </View>

        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image
            style={styles.image}
            resizeMode="contain"
            source={{
              uri:
                insident && insident.media && insident.media.image_url
                  ? insident && insident.media && insident.media.image_url
                  : null,
            }}
          />
        </View>
        {this.state.loading && (
          <View>
            <ActivityIndicator size="small" color="red" />
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  IncidentReducerState: state.IncidentReducer,
});

const mapDispatchToProps = dispatch => ({
  insidentupdate: data => {
    dispatch(insidentupdate(data));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RegisterContainer);
