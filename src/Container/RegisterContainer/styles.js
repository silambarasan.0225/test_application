import {StyleSheet} from 'react-native';
import {Colors, Fonts} from './../../Theme';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
  },

  login: {
    height: 60,
    width:"100%",
    justifyContent: 'center',
    paddingLeft: 5,
    margin: 2,
    textDecorationLine: 'underline',

  },
  underline:{
    textDecorationLine: 'underline'
  },
  gender: {
    height: 50,
    width: '100%',
    // backgroundColor: Colors.text,
  },
  image: {
    width: '100%',
    height: '100%',
    alignItems:"center",
    justifyContent:"center",
    borderRadius: 10,
    resizeMode: 'contain',

  },
  email_username: {
    height:60,
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 10,
    margin: 2,
  },
  headertitle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  star: {
    color: Colors.redcoler,
  },
  password: {
    height: 40,
    alignItems: 'center',
    marginTop: 10,
    flexDirection: 'row',
    margin: 2,
  },
  textview: {
    height: 40,
    width: '100%',
    alignItems: 'center',
    borderRadius: 5,
    paddingLeft: 5,
    backgroundColor: Colors.text,
    flexDirection: 'row',
    margin: 2,
  },
  engine: {
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    width: '100%',
  },
  row: {
    padding: 15,
    marginBottom: 5,
  },
  button: {
    marginTop: 50,
    height: 50,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 3,
    paddingLeft: 5,
    backgroundColor: Colors.button,
    flexDirection: 'row',
    margin: 2,
  },
  input: {
    height: 40,
    width: '100%',
    padding: 5,
  },
});
