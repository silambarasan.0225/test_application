/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {View, ActivityIndicator} from 'react-native';

import {styles} from './styles';
import NetInfo from '@react-native-community/netinfo';

class LauncherContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }

  componentDidMount() {
    this.setState({loading: true});
    this.CheckConnectivity();
  }
  CheckConnectivity = () => {
    NetInfo.addEventListener(state => {
      if (state && state.isConnected) {
        this.moveToNav();
      } else {
        alert('Please check the internet connection ?');
      }
    });
  };

  moveToNav = async () => {
    this.props.navigation.navigate('Nav');
    this.setState({loading: false});
  };
  render() {
    return (
      this.state.loading && (
        <View style={styles.container}>
          <ActivityIndicator size="small" color="red" />
        </View>
      )
    );
  }
}

export default LauncherContainer;
