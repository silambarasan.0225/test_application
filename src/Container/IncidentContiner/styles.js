import {StyleSheet} from 'react-native';
import {Colors} from '../../Theme';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    height: 40,
    width: '100%',
    padding: 10,
  },
  headertitle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textdata: {
    height: 40,
    alignItems: 'flex-start',
    justifyContent: 'center',
    width: '100%',
    backgroundColor: 'pink',
    marginTop: 5,
    borderRadius: 5,
  },
  tinyLogo: {
    width: 20,
    height: 20,
  },
  listdata: {
    height: 40,
    flex: 1,
    justifyContent: 'center',
  },
  refresh: {
    height: 40,
    width: '100%',
    paddingRight: 10,
    alignItems: 'center',
    justifyContent: 'center',

    borderRadius: 10,
  },
  image: {
    width: '100%',
    height: '100%',
    padding: 10,
    marginTop: 20,
    marginBottom: 20,
    borderRadius:10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  listtext: {
    padding: 10,
  },
  button: {
    height: 40,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.button,
    borderRadius: 5,
  },
  refbutton: {
    height: 40,
    width: '100%',
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.button,
    borderRadius: 5,
  },
});
