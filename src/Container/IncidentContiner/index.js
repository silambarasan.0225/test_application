/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  ActivityIndicator,
  Image,
  FlatList,
} from 'react-native';

import {styles} from './styles';
import {Fonts, Colors} from '../../Theme';
import colors from '../../Theme/Colors';
import axios from 'axios';
import {url} from '../../Service/Configuration/config';

//redux
import {connect} from 'react-redux';
import {updateUserComment} from './../../Redux/Actione/FeedCommentActions';

const {
  mont_semibold_h5,
  mont_semibold_h4,
  mont_semibold_h6,
  mont_semibold_h1,
} = Fonts.style;
const {button, white} = Fonts.colorStyle;

const RenderItem = props => {
  return (
    <TouchableOpacity
      style={{
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 5,
        backgroundColor: colors.text,
        flexDirection: 'row',
        height: 90,
      }}
      onPress={() => props.deletitem(props.data)}>
      {props.data.media.image_url ? (
        <View
          style={{
            height: '100%',
            width: 90,
            borderRadius: 10,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#D7D2D0',
          }}>
          <Image
            style={styles.image}
            resizeMode="contain"
            source={{
              uri: props.data.media.image_url
                ? props.data.media.image_url
                : null,
            }}
          />
        </View>
      ) : null}

      <View style={{flex: 1}}>
        <View style={styles.listdata}>
          <Text numberOfLines={2} style={[mont_semibold_h5, styles.listtext]}>
            {props.data.title}
          </Text>
        </View>
        <View style={styles.listdata}>
          <Text style={[mont_semibold_h6, styles.listtext]}>
            {'ID : ' + props.data.id}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const Loadedata = props => {
  let renderData = props.datas;

  return (
    <View style={{flex: 1}}>
      <View style={{flex: 1, paddingBottom: 10}}>
        <FlatList
          data={renderData}
          initialNumToRender={10}
          maxToRenderPerBatch={1}
          windowSize={7}
          maxToRenderPerBatch={100}
          renderItem={({item, index}) => (
            <RenderItem data={item} deletitem={props.deletitem} />
          )}
          keyExtractor={() => props.keyExtractor}
        />
      </View>
    </View>
  );
};

class IncidentContiner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }

  async componentDidMount() {
    this.setState({loading: true});
    try {
      const response = await axios.get(
        url + '/api/v2/incidents?page=1&proximity_square=100',
      );
      this.props.updateUserComment(response.data.incidents);

      this.setState({
        loading: false,
      });
    } catch (error) {
      // handle error
      alert(error.message);
    }
  }

  deletitem = listdata => {
    this.props.navigation.navigate('RegisterContainer', {listdata: listdata});
  };

  extractKey = ({id}) => id;

  render() {
    const {comment} = this.props.commentReducerState;

    return (
      <View style={styles.container}>
        <Loadedata
          keyExtractor={this.extractKey}
          datas={comment}
          onFormSubmit={this.ViewData}
          renderItem={this.renderItem}
          deletitem={this.deletitem}
        />
        {this.state.loading && (
          <View>
            <ActivityIndicator size="small" color="red" />
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  commentReducerState: state.commentReducer,
});

const mapDispatchToProps = dispatch => ({
  updateUserComment: data => {
    dispatch(updateUserComment(data));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(IncidentContiner);
