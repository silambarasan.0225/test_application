import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

//Reducers
import CommentReducer from './src/Redux/Reducer/CommentReducer';
import IncidentReducer from './src/Redux/Reducer/IncidentoReducer';
import LocationReducer from './src/Redux/Reducer/LocationReducer';

const AppReducer = combineReducers({
  commentReducer: CommentReducer,
  IncidentReducer: IncidentReducer,
  LocationReducer: LocationReducer,
});

const store = createStore(AppReducer, applyMiddleware(thunk));

export default store;
