/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {SafeAreaView, StyleSheet, View, Text, StatusBar} from 'react-native';

import AppNavigator from './src/Navigation/Routing';
import {NavigationContainer} from '@react-navigation/native';

import codePush from 'react-native-code-push';
import NetInfo from '@react-native-community/netinfo';
import {useNetInfo} from '@react-native-community/netinfo';

import {Provider} from 'react-redux';
import store from './store';

class App extends Component {
  componentDidMount() {
    // this.CheckConnectivity();
    codePush.sync({
      updateDialog: true,
      installMode: codePush.InstallMode.IMMEDIATE,
    });
  }

  CheckConnectivity = () => {
    NetInfo.addEventListener(state => {
      if (state && !state.isConnected) {
        alert('Please check the internet connection ?');
      }
    });
  };

  render() {
    return (
      <Provider store={store}>
        <NavigationContainer>
          <AppNavigator />
        </NavigationContainer>
      </Provider>
    );
  }
}

let codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_START,
  installMode: codePush.InstallMode.IMMEDIATE,
};

//code push on
export default (App = codePush(codePushOptions)(App));

// export default App
